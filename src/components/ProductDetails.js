import React from "react";

const ProductDetails = ({ product }) => {
  return (
    <div className="product-details">
      <h2>{product.title}</h2>
      <img src={product.image} alt={product.title} />
      <p>Category: {product.category}</p>
      <p>Description: {product.description}</p>
      <p>Price: ${product.price}</p>
      <p>
        Rating: {product.rating.rate}, {product.rating.count}
      </p>
    </div>
  );
};

export default ProductDetails;
