// ShoppingCart.js
import React from "react";
import "../styles/ShoppingCart.scss";

const ShoppingCart = ({ cartItems, removeFromCart, updateCart }) => {
  const calculateTotalPrice = () => {
    return cartItems.reduce(
      (total, item) => total + item.price * item.quantity,
      0
    );
  };

  return (
    <div className="shopping-cart">
      <h2>Shopping Cart</h2>
      {cartItems.map((item) => (
        <div key={item.productId} className="cart-item">
          <p>{item.name}</p>
          <p>Price: ${item.price}</p>
          <p>Quantity: {item.quantity}</p>
          <button onClick={() => updateCart(item.productId, item.quantity + 1)}>
            +
          </button>
          <button
            onClick={() =>
              updateCart(
                item.productId,
                item.quantity > 0 ? item.quantity - 1 : 0
              )
            }
          >
            -
          </button>
          <button onClick={() => removeFromCart(item.productId)}>Remove</button>
        </div>
      ))}
      <p>Total: ${calculateTotalPrice()}</p>
    </div>
  );
};

export default ShoppingCart;
