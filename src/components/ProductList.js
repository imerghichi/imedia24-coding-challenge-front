import React, { useState } from "react";
import "../styles/ProductList.scss";
import ProductDetails from "./ProductDetails";

const ProductList = ({ products, addToCart }) => {
  const [selectedProduct, setSelectedProduct] = useState(null);

  const openProductDetails = (product) => {
    setSelectedProduct(product);
  };

  const closeProductDetails = () => {
    setSelectedProduct(null);
  };

  return (
    <div className="product-list">
      {products.map((product) => (
        <div key={product.id} className="product">
          <img src={product.image} alt={product.title} />
          <h3>{product.title}</h3>
          <p>Price: ${product.price}</p>
          <p>Rating: {product.rating.rate}</p>
          <button onClick={() => addToCart(product)}>Add to Cart</button>
          <button onClick={() => openProductDetails(product)}>
            Show Details
          </button>
        </div>
      ))}
      {selectedProduct && (
        <div className="product-details-overlay">
          <div className="product-details-popup">
            <ProductDetails product={selectedProduct} />
            <button onClick={closeProductDetails}>Close</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductList;
