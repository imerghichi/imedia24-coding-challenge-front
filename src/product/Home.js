// Home.js
import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from "react-router-dom";
import ShoppingCart from "../components/ShoppingCart";
import ProductListPage from "./ProductListPage";

const Home = () => {
  return (
    <div className="home">
      <Router>
        <header>
          <h1>Welcome to our Online Store</h1>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/cart">Shopping Cart</Link>
              </li>
            </ul>
          </nav>
        </header>
        <Switch>
          <Route exact path="/">
            <ProductListPage />
          </Route>
          <Route path="/cart">
            <ShoppingCart />
          </Route>
          <Redirect to="/" />
        </Switch>
        <footer>
          <p>Contact us at example@store.com</p>
        </footer>
      </Router>
    </div>
  );
};

export default Home;
