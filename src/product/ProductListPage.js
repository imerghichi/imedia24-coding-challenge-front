import React, { useState } from "react";
import ProductList from "../components/ProductList";
import ShoppingCart from "../components/ShoppingCart";

const ProductListPage = () => {
  products = [
    {
      productId: 1,
      name: "test",
      image: "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
      price: 23,
      rating: { rate: 3.9, count: 120 }
    },
    {
      productId: 2,
      name:
        "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg",
      price: 25,
      rating: { rate: 3.9, count: 120 }
    }
  ];
  const [cart, setCart] = useState([]);

  const addToCart = (product) => {
    const existingProduct = cart.find(
      (item) => item.productId === product.productId
    );
    if (existingProduct) {
      const updatedCart = cart.map((item) =>
        item.productId === product.productId
          ? { ...item, quantity: item.quantity + 1 }
          : item
      );
      setCart(updatedCart);
    } else {
      setCart([...cart, { ...product, quantity: 1 }]);
    }
  };

  const removeFromCart = (productId) => {
    const updatedCart = cart.filter((item) => item.productId !== productId);
    setCart(updatedCart);
  };

  const updateCart = (productId, newQuantity) => {
    const updatedCart = cart.map((item) =>
      item.productId === productId ? { ...item, quantity: newQuantity } : item
    );
    setCart(updatedCart);
  };

  return (
    <div>
      <h1>Product List</h1>
      <ProductList products={products} addToCart={addToCart} />
      <ShoppingCart
        cartItems={cart}
        removeFromCart={removeFromCart}
        updateCart={updateCart}
      />
    </div>
  );
};

export default ProductListPage;
