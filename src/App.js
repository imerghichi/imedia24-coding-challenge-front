import ShoppingCart from "./components/ShoppingCart";
import Home from "./product/Home";
import ProductListPage from "./product/ProductListPage";
import "./styles.css";

export default function App() {
  return <ProductListPage />;
}
